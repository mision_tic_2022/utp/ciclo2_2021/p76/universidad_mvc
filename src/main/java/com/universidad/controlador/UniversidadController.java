package com.universidad.controlador;

import java.util.ArrayList;

import com.universidad.modelo.Universidad;

public class UniversidadController {
    
    //private Universidad[] universidades;
    private ArrayList<Universidad> arrayUniversidad;

    public UniversidadController(){
        //this.universidades = new Universidad[cant_universidades];
        this.arrayUniversidad = new ArrayList<Universidad>();
    }

    public Universidad getUniversidad(int pos){
        //return this.universidades[pos];
        return arrayUniversidad.get(pos);
    }
    /*
    public Universidad[] getUniversidades() {
        return universidades;
    }
    */

    public ArrayList<Universidad> getArrayUniversidad(){
        return this.arrayUniversidad;
    }

    public void setArrayUniversidad(ArrayList<Universidad> arrayUniversidad){
        this.arrayUniversidad = arrayUniversidad;
    }
    /*
    public void setUniversidades(Universidad[] universidades) {
        this.universidades = universidades;
    }
    */

    public void setUniversidad(Universidad universidad, int pos){
        //this.universidades[pos] = universidad;
        this.arrayUniversidad.set(pos, universidad);
    }

    public void crear_universidad(String nombre, String direccion, String nit){
        //this.universidades[pos] = new Universidad(nombre, direccion, nit);
        Universidad objUniversidad = new Universidad(nombre, direccion, nit);
        this.arrayUniversidad.add(objUniversidad);
    }

}
